/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.tests;
import assignment2javaoct27.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.sql.SQLException;
import java.sql.Timestamp;
/**
 *
 * @author cha_xi
 */
public class unittest {
    private MedBean medB;
    private Patient patient;
    private InPatientBean inPatientB;
    private SurgicalBean surgicalB;
    private PatientDAOImpl pDAOImpl;
    
    @Before
    public void unittest(){
        Timestamp dateOfMed = Timestamp.valueOf("2000-2-2 1:1:1");
        Timestamp admissionDate = Timestamp.valueOf("1999-3-3 23:59:59");
        Timestamp releaseDate = Timestamp.valueOf("2001-4-4 2:2:2");
        Timestamp dateOfStay = Timestamp.valueOf("2002-5-5 8:8:8");
        Timestamp dateOfSurgery = Timestamp.valueOf("2009-6-6 9:9:9");
        
        medB = new MedBean(-1, -2, dateOfMed, "medtest", 0.1, 0.2);
        patient = new Patient(-11, "LastNameTest", "FirstNameTest", "TestDiagnosis", admissionDate, releaseDate);
        inPatientB = new InPatientBean(-22, -33, dateOfStay, "AAA2002BB", 0.01, 1.01, 2.33);
        surgicalB = new SurgicalBean(-111, -222, dateOfSurgery, "surgery", 100.1, 10.1, 1.99);
        
        pDAOImpl = new PatientDAOImpl();
    }
    /**
     * 
     * insert a patient and find it by another pDAOImpl
     * delete a patient and find it by another pDAOImpl
     * @throws SQLException 
     */
    @Test
    public void Test_Patient() throws SQLException{
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        //System.out.println("find 1st patient:/n"+pDAOImpl.findById(1).toString());
        System.out.println("insert Patient:"+patient.toString()+"\n result: "+
                pDAOImpl2.insertPatient(patient));
        assertEquals(patient,pDAOImpl.findById(patient.getPatientID()));
        //delete patient and find by another pDAOImpl
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int idPatient = patient.getPatientID();
        pDAOImpl3.deletePatient(patient);
        assertNull(pDAOImpl3.findById(idPatient));   
    }
    /**
     * test for InPatientBean
     * insert and find
     * delete and find
     * @throws SQLException 
     */
    @Test
    public void Test_InPatientBean() throws SQLException{
        //insert and find
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        pDAOImpl2.insertInp(inPatientB);
        assertEquals(inPatientB,pDAOImpl.findById(inPatientB.getId()));
        //delete and find
        pDAOImpl.deleteMed(inPatientB.getId());
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int id = inPatientB.getId();
        assertNull(pDAOImpl3.findById(id));
      
    }
    /**
     * test for MedBean
     * insert and find
     * delete and find
     */
    @Test
    public void Test_MedBean() throws SQLException{
        //insert and find
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        pDAOImpl2.insertMed(medB);
        assertEquals(medB,pDAOImpl.findMedBeanById(medB.getID()));
        //delete and find
        pDAOImpl.deleteMed(medB.getID());
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int id = medB.getID();
        assertNull(pDAOImpl3.findMedBeanById(id));
    }
    
    
}
