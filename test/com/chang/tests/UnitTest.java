/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.tests;
import assignment2javaoct27.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
/**
 *
 * @author cha_xi
 */
public class UnitTest {
        private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "concordia";
    ////////////////////
    private MedBean medB;
    private Patient patient;
    private InPatientBean inPatientB;
    private SurgicalBean surgicalB;
    private PatientDAOImpl pDAOImpl;
    
    @Before
    public void UnitTest(){
        Timestamp dateOfMed = Timestamp.valueOf("2000-2-2 1:1:1");
        Timestamp admissionDate = Timestamp.valueOf("1999-3-3 23:59:59");
        Timestamp releaseDate = Timestamp.valueOf("2001-4-4 2:2:2");
        Timestamp dateOfStay = Timestamp.valueOf("2002-5-5 8:8:8");
        Timestamp dateOfSurgery = Timestamp.valueOf("2009-6-6 9:9:9");
        
        medB = new MedBean(-1, -2, dateOfMed, "medtest", 0.1, 0.2);
        patient = new Patient(-11, "LastNameTest", "FirstNameTest", "TestDiagnosis", admissionDate, releaseDate);
        inPatientB = new InPatientBean(-22, -33, dateOfStay, "AAA2002BB", 0.01, 1.01, 2.33);
        surgicalB = new SurgicalBean(-111, -222, dateOfSurgery, "surgery", 100.1, 10.1, 1.99);
        
        pDAOImpl = new PatientDAOImpl();
    }
    /**
     * 
     * insert a patient and find it by another pDAOImpl
     * delete a patient and find it by another pDAOImpl
     * @throws SQLException 
     */
    @Test
    public void Test_Patient() throws SQLException{
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        //System.out.println("find 1st patient:/n"+pDAOImpl.findById(1).toString());
        System.out.println("insert Patient:"+patient.toString()+"\n result: "+
                pDAOImpl2.insertPatient(patient));
        assertEquals(patient,pDAOImpl.findById(patient.getPatientID()));
        //delete patient and find by another pDAOImpl
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int idPatient = patient.getPatientID();
        pDAOImpl3.deletePatient(patient);
        assertNull(pDAOImpl3.findById(idPatient));   
    }
    /**
     * test for InPatientBean
     * insert and find
     * delete and find
     * @throws SQLException 
     */
    @Test
    public void Test_InPatientBean() throws SQLException{
        //insert and find
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        pDAOImpl2.insertInp(inPatientB);
        assertEquals(inPatientB,pDAOImpl.findById(inPatientB.getId()));
        //delete and find
        pDAOImpl.deleteMed(inPatientB.getId());
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int id = inPatientB.getId();
        assertNull(pDAOImpl3.findById(id));
      
    }
    /**
     * test for MedBean
     * insert and find
     * delete and find
     */
    @Test
    public void Test_MedBean() throws SQLException{
        //insert and find
        PatientDAOImpl pDAOImpl2 = new PatientDAOImpl();
        pDAOImpl2.insertMed(medB);
        assertEquals(medB,pDAOImpl.findMedBeanById(medB.getID()));
        //delete and find
        pDAOImpl.deleteMed(medB.getID());
        PatientDAOImpl pDAOImpl3 = new PatientDAOImpl();
        int id = medB.getID();
        assertNull(pDAOImpl3.findMedBeanById(id));
    }
    
    /**
     * This routine recreates the database for every test. This makes sure that
     * a destructive test will not interfere with any other test.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss who helped me out last winter with an issue with Arquillian. Look
     * up Arquillian to learn what it is.
     */
    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("createFishMySQL.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
